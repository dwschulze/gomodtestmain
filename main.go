package main

import (
	"fmt"
	"bitbucket.org/dwschulze/gomodtest0"
	"bitbucket.org/dwschulze/gomodtest1/subdir1"
	"bitbucket.org/dwschulze/gomodtest2/subdir1/subdir2"
)

func main() {
	
	fmt.Println("main")
	fmt.Println(gomodtest0.PingGoModTest0())
	fmt.Println(gomodtest1.PingGoModTest1())
	fmt.Println(gomodtest2.PingGoModTest2())
}