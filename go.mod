module bitbucket.org/dwschulze/gomodtestmain

go 1.15

require (
	bitbucket.org/dwschulze/gomodtest0 v0.0.1 // indirect
	bitbucket.org/dwschulze/gomodtest1/subdir1 v0.0.1 // indirect
	bitbucket.org/dwschulze/gomodtest2/subdir1/subdir2 v0.0.2 // indirect
)
